# == Schema Information
#
# Table name: people
#
#  id          :integer          not null, primary key
#  first_name  :string           not null
#  middle_name :string
#  last_name   :string           not null
#  email       :string           not null
#  age         :integer          not null
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#
class Person < ApplicationRecord
  has_paper_trail
  
  validates :first_name, presence: true
  validates :last_name, presence: true
  validates :email, presence: true
  validates :age, presence: true
end
