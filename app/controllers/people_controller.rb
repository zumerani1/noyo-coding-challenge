class PeopleController < ApplicationController
  
  # Without the line below, several of the calls below will fail (e.g. GET - `create`).
  # The reason they fail is because the client is not passing a valid CSRF token. Using
  # `form_for` should pass an auth token and would ultimately mean that we may not need
  # the line below. For testing purposes (via Postman), I've added the following line for
  # above reasons.
  skip_before_action :verify_authenticity_token

  '''
    Method: POST
    URL: /people
  '''
  def create
    person = Person.create!(person_params)
    render json: person, status: 201
  rescue => ex
    render json: { error: "exception: #{ex}" }, status: 500
  end

  '''
    Method: GET
    URL: /people
  '''
  def index
    people = Person.all
    render json: people, status: 201
  end

  '''
    Method: GET
    URL: /people/:id
  '''
  def show
    id = params[:id]
    person = Person.find(id)

    render json: person, status: 201
  rescue => ex
    render json: { error: "Exception: #{ex}" }, status: 500
  end

  '''
    Method: PUT
    URL: /people/:id
  '''
  def update
    person = Person.find(params[:id])
    person.update!(person_params)

    render json: person, status: 201
  rescue => ex
    render json: { error: "Exception: #{ex}" }, status: 500
  end

  '''
    Method: GET
    URL: /people/:id/version/:version_id
  '''
  def get_person_from_version
    # If only one version exists, it must mean that the event type was 'create' - if
    # that is the case, simply return that record. If not, go through the changes and 
    # construct a payload that represents what that record looked like at that 
    # time (version ID).

    person = Person.find(params[:id])
    if person.versions.count == 1
      render json: person, status: 201
    else
      version = PaperTrail::Version.find_by(item_id: params[:id], id: params[:version_id])
      payload = version.reify&.attributes || {}

      # Calling `changeset` will yield the following hash:
      # { first_name => [previous_value, new_value], last_name => [previous_value, new_value], ... }
      # 
      # The idea is that we go through the changeset hash and build a payload that yields what the 
      # object looked like at this time.
      version.changeset.each do |key, value|
        payload.merge!(key => value.second)
      end

      render json: payload, status: 201
    end

  rescue => ex
    render json: { error: "Exception: #{ex}" }, status: 500
  end

  '''
    Method: DELETE
    URL: /people/:id
  '''
  def destroy
    person = Person.find(params[:id])
    if person
      person.destroy! if person
      render json: person, status: :ok
    end
  rescue => ex
    render json: { error: "Error: #{ex}", success: false }, status: 500
  end

  '''
    Method: GET
    URL: /history/people/:id
  '''
  def get_all_versions
    person = Person.find(params['id'])

    if person
      render json: person.versions, status: 201
    end
  rescue => ex
    render json: { error: "Error: #{ex}", success: false }, status: 500
  end

  private

  def person_params
    # To ensure that we have the correct params sent, we first call `require` to 
    # make sure that attributes (first_name, last_name, etc.) are encapsulated
    # in a `person` object. Next, we call `permit` to return *only* the keys listed.
    params.require(:person).permit(:first_name, :last_name, :middle_name, :email, :age)
  end
end