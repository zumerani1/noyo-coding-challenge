class CreatePeople < ActiveRecord::Migration[6.1]
  def change
    create_table :people do |t|
      t.string :first_name, null: false
      t.string :middle_name, null: true
      t.string :last_name, null: false
      t.string :email, null: false
      t.integer :age, null: false

      t.timestamps
    end
  end
end
