# Noyo Take Home Coding Challenge - Person API

## Requirements
1. Node / NPM
2. Docker

## Installation

### Docker
1. `cd noyo-coding-challenge`

2. `docker-compose build` (this will likely take time to build - at most 5-10 minutes)

3. `docker-compose up`

#### In a separate tab, run the following:

1. `docker-compose run web bundle exec rails generate paper_trail:install --with-changes`

2. `docker-compose run web rake db:migrate`

With the app already running, head over to localhost:3000 - you should see a "Welcome to Rails" Page! From here, you can 
take a look at the different endpoints below to make calls to our API.

### Bare Metal

#### Download Homebrew
1. Download Homebrew - `ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"`
2. `brew update`

#### Install Ruby Version Manager (RVM) and Ruby
1. Install RVM: `\curl -sSL https://get.rvm.io | bash -s stable --ruby`
2. `source /Users/<your-username>/.rvm/scripts/rvm`
3. We are using ruby-2.6.5, to install: `rvm install 2.6.5`
4. To use ruby-2.6.5 as the default, run: `rvm --default use 2.6.5`

#### Install Rails
1. `gem install rails`

#### Install Yarn
1. `npm install --global yarn`

#### Run App
1. `bundle install`
2. `rake db:migrate`
3. `rails s` (runs server)

With the app already running, head over to localhost:3000 - you should see a "Welcome to Rails" Page! From here, you can 
take a look at the different endpoints below to make calls to our API.

## Endpoints

1. 
```
METHOD: POST (create)
URL: /people
Body:
{
	"person": {
	   "first_name": "Bob",
	   "last_name": "Miller",
	   "email": "bmiller@gmail.com",
	   "age": 30
     }
}
Description - Builds and saves a Person record with the above parameters
```
<br />

2. 
```
METHOD: PUT (update)
URL: /people/:id
Body:
{
	"person": {
	   "email": "bmiller101@gmail.com",
	   "age": 33
     }
}
Description - Updates the record given the ID and returns the updated record.
```
<br />
3. 
```
METHOD: GET
URL: /people
Description - Retrieves all Person records from the database.
```
<br />
4. 
```
METHOD: GET
URL: /people/:id
Body:
{
	"id": :id
}
Description - Retrieves a Person record with the given ID. If no record exists, an error payload is sent back.
```
<br />
5. 
```
METHOD: GET
URL: /people/:id/version/:version_id
Body:
{
	"id": :id,
	"version_id": :version_id
}
Description - Returns a Person object based on what the attributes were given the version_id.
```
<br />
6. 
```
METHOD: DELETE
URL: /people/:id
Body:
{
	"id": :id
}
Description - Deletes the Person record associated with the ID, and returns the deleted object.
```
<br />
7. 
```
METHOD: GET
URL: /history/people/:id
Body:
{
	"id": :id
}
Description - Retrieves all the versions of a Person record based on the ID.
```

## Running Tests

### Docker

`docker-compose run web rspec spec/controllers/people_controller_spec.rb`

### Bare Metal

`rspec spec/controllers/people_controller_spec.rb`