Rails.application.routes.draw do
  resources :people

  get '/people/:id/version/:version_id', to: 'people#get_person_from_version'
  get '/history/people/:id', to: 'people#get_all_versions'
end
