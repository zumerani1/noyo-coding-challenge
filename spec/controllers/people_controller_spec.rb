require 'rails_helper'

RSpec.describe PeopleController, type: :controller do
  describe 'people_controller' do
    def execute_post_request(method, request_body)
      post(
        method,
        params: request_body
      )

      JSON.parse(response.body, object_class: OpenStruct)
    end

    def execute_get_request(method, request_body = nil)
      get(
        method,
        params: request_body
      )

      JSON.parse(response.body, object_class: OpenStruct)
    end

    def execute_put_request(method, request_body)
      put(
        method,
        params: request_body
      )

      JSON.parse(response.body, object_class: OpenStruct)
    end

    describe '#create' do
      context 'when passing valid input' do
        it 'Person object is created successfully' do
          payload = {
            'person': {
              'first_name': Faker::Name.first_name,
              'last_name': Faker::Name.last_name,
              'email': Faker::Internet.email,
              'age': Faker::Number.number(digits: 3),
            }
          }

          resp = execute_post_request(:create, payload)
          expect(resp.first_name).to eq payload[:person][:first_name]
          expect(resp.last_name).to eq payload[:person][:last_name]
          expect(resp.email).to eq payload[:person][:email]
          expect(resp.age).to eq payload[:person][:age]

          expect do
            execute_post_request(:create, payload)
          end.to change { Person.count }.by(1)
        end
      end

      context 'when passing invalid input' do
        it 'Person object is *not* created successfully' do
          payload = {
            'person': {
              'first_name': Faker::Name.first_name,
              'email': Faker::Internet.email,
              'age': Faker::Number.number(digits: 3),
            }
          }

          resp = execute_post_request(:create, payload)
          expect(resp.error).to include "Last name can't be blank"
        end
      end
    end

    describe '#index' do
      it 'yields all the Person records available in the database' do
        5.times do
          FactoryBot.create :person
        end

        resp = execute_get_request(:index)
        
        expect(resp.count).to eq 5
        expect(resp.map(&:id)).to eq [1, 2, 3, 4, 5]
      end
    end

    describe '#show' do
      let(:person) { FactoryBot.create :person }
      context 'when record exists' do
        it 'the record is successfully returned' do
          payload = { 'id': person.id}
          resp = execute_get_request(:show, payload)
          expect(resp.first_name).to eq person.first_name
          expect(resp.last_name).to eq person.last_name
          expect(resp.email).to eq person.email
          expect(resp.age).to eq person.age
        end
      end

      context 'when record does not exist' do
        it 'the response yields an error explaining that record does not exist' do
          payload = { 'id': person.id + 1 }
          resp = execute_get_request(:show, payload)
          expect(resp.error).to include "Couldn't find Person with "
        end
      end
    end

    describe '#update' do
      context 'when updating a valid, existing record' do
        let(:person) { FactoryBot.create :person }
        it 'record is updated successfully' do
          update_payload = {
            'id': person.id,
            'person': {
              'first_name': Faker::Name.first_name,
              'last_name': Faker::Name.last_name,
              'email': Faker::Internet.email,
              'age': Faker::Number.number(digits: 3),
            }
          }

          resp = execute_put_request(:update, update_payload)
          expect(resp.first_name).to eq update_payload[:person][:first_name]
          expect(resp.last_name).to eq update_payload[:person][:last_name]
          expect(resp.email).to eq update_payload[:person][:email]
          expect(resp.age).to eq update_payload[:person][:age]
        end

        it 'version is updated successfully' do
          update_payload = {
            'id': person.id,
            'person': {
              'first_name': Faker::Name.first_name,
              'last_name': Faker::Name.last_name,
              'email': Faker::Internet.email,
              'age': Faker::Number.number(digits: 3),
            }
          }
          expect do
            execute_post_request(:update, update_payload)
          end.to not_change { Person.count }
            .and change { person.reload.versions.count }.by(1)
        end
      end

      context 'when attempting to update a record that does not exist' do
        let(:person) { FactoryBot.create :person }
        it 'error response explains that record does not exist' do
          update_payload = {
            'id': person.id + 1,
            'person': {
              'first_name': Faker::Name.first_name,
              'last_name': Faker::Name.last_name,
              'email': Faker::Internet.email,
              'age': Faker::Number.number(digits: 3),
            }
          }

          resp = execute_put_request(:update, update_payload)
          expect(resp.error).to include "Couldn't find Person with "
        end
      end
    end

    describe '#get_person_from_version' do
      before do
        @person = FactoryBot.create :person
        
        # Update `person` so that `person.versions` is > 1
        person.update!(first_name: 'first', last_name: 'last')
        @old_person = Person.find(person.id)
        @version = person.versions.last
        person.update!(first_name: 'first_two', last_name: 'last_two')

        person
      end

      context 'when record exists' do
        let(:person) { Person.find(@person.id) }
        it 'returns the record at that version' do
          payload = {
            'id': person.id,
            'version_id': @version.id,
          }

          resp = execute_get_request(:get_person_from_version, payload)
          expect(resp.first_name).to eq @old_person.first_name
          expect(resp.last_name).to eq @old_person.last_name
          expect(resp.email).to eq @old_person.email
          expect(resp.age).to eq @old_person.age
        end
      end

      context 'when *only one* version exists' do
        let(:person) { FactoryBot.create :person }

        it 'return that one record' do
          payload = {
            'id': person.id,
            'version_id': person.versions.last.id,
          }

          resp = execute_get_request(:get_person_from_version, payload)
          expect(resp.first_name).to eq person.first_name
          expect(resp.last_name).to eq person.last_name
          expect(resp.email).to eq person.email
          expect(resp.age).to eq person.age
        end
      end
    end

    describe '#get_all_versions' do
      let(:person) do
        person = FactoryBot.create :person
        person.update!(first_name: 'Bob', last_name: 'Miller')

        person
      end

      it 'returns all the versions for a specific record' do
        payload = {
          'id': person.id,
        }

        resp = execute_get_request(:get_all_versions, payload)
        expect(person.versions.count).to eq 2
      end
    end
  end
end