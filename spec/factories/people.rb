# == Schema Information
#
# Table name: people
#
#  id          :integer          not null, primary key
#  first_name  :string           not null
#  middle_name :string
#  last_name   :string           not null
#  email       :string           not null
#  age         :integer          not null
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#
require 'faker'

FactoryBot.define do
  factory :person do
    first_name { Faker::Name.first_name }
    middle_name { Faker::Name.middle_name }
    last_name { Faker::Name.last_name }
    email { Faker::Internet.email }
    age { Faker::Number.number(digits: 3) }
  end
end
